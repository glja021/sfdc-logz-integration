public class LogzApiMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/plain; charset=UTF-8');
        res.setStatusCode(200);
        return res;
    }
}