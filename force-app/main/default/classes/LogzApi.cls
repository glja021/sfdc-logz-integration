public class LogzApi {
    String key;
    
    public LogzApi(String key){
        this.key = key;
    }
    
    public Id sendLogs(List<Object> objectsToLog) {
        try {
            if(objectsToLog.size() == 0) return null;
            String httpBody = '';
            for(Object objekt : objectsToLog) {
            	httpBody += JSON.serialize(objekt) + '\n';   
            }
            return System.enqueueJob(new LogzApiImplementation(httpBody, key));
        } catch(Exception ex) {
            System.debug(System.LoggingLevel.ERROR, ex);
            return null;
        }
    }
    
    class LogzApiImplementation implements Queueable, Database.AllowsCallouts {
        String body;
        String key;
        
        public LogzApiImplementation(String body, String key){
        	this.key = key;
            this.body = body;
        }
        
    	public void execute(QueueableContext context) {
            try {
                HttpRequest req = new HttpRequest();
                req.setEndpoint('callout:Logz?type=http-bulk&token=' + EncodingUtil.URLENCODE(key,'UTF-8'));
                req.setMethod('POST');
                req.setBody(body);
                System.debug(body);   
                Http http = new Http();
                HTTPResponse res = http.send(req);
                System.debug(res.getStatus());   
            } catch(Exception ex) {
                System.debug(System.LoggingLevel.ERROR, ex);
            }    
        }
    }
}