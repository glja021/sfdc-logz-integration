@isTest public class LogzApiTest {
    @isTest static void sendLogsTest(){
        //Arrange
        Test.setMock(HttpCalloutMock.class, new LogzApiMock());
        LogzApi logzApiClient = new LogzApi('_____________');
        List<String> testObjects = new List<String> {
          'Item 1',
          'Item 2',
          'Item 3',
          'Item 4',
          'Item 5',
          'Item 6',
          'Item 7',
          'Item 8'
        };
        String jobId = null;
            
        //Act
        Test.startTest();
        jobId = logzApiClient.sendLogs(testObjects);
        Test.stopTest();
        
        //Assert
        System.assertNotEquals(null, jobId);
    }
}