# Apex integration with Logz.io

The purpose of this repo is to develop a Salesforce wrapper to upload logs into [Logz.io](https://logz.io/)

# Salesforce quota

Every call to the wrapper executes 1 Asyncronous Job and 1 Callout, so keep in mind the restrictions that salesforce imposes

# Token

You can get a token for your Logz instance [here](https://app.logz.io/#/dashboard/settings/general) and then copy your key in *LogzApiKey.ApiKey__c* custom metadata  

# Usage
It accepts a list of any object that can be serialized using the JSON class
``` java
    List<String> myLogs = new List<String>{
      "This is a demo log",
      "This is another demo log"
    };
    LogzApi logzApiClient = new LogzApi(REPLACE_WITH_YOUR_TOKEN);
    logzApiClient.sendLogs(myLogs);
```

# To deploy it
Login in your sandbox with the following command
```
    sfdx force:auth:web:login -r https://test.salesforce.com -a myAlias
```
and then deploy it to the Org
```
    sfdx force:source:deploy --sourcepath
```